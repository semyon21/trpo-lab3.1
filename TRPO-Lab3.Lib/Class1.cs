﻿using System;

namespace TRPOLab3Lib
{
   
    public class Class1
    {
        public double OutR;
        public double InR;
        double a;

        public static double GetSq(double OutR, double InR, double a)
        {

            if (OutR < InR)
            {
                throw new ArgumentException("Внешний радиус не может быть меньше внутренненго!");
            }
            double S = Math.PI * (a / 360) * (Math.Pow(OutR, 2) - Math.Pow(InR, 2));
            return Math.Round(S, 2);
        }

    }
}
